#(set-default-paper-size "a5landscape")
\paper {
left-margin = 0
right-margin = 30
indent=0
}
%#(set-global-staff-size 18)

global = {
  \key a \minor
  \time 4/4
}

intro = <<
    \relative {
    % intro
        %\mark "Intro"
        a'4. b8 c4 a | c8 c b a b4 e, |
        b'4. c8 d4 b8 r16 b | e8 d c b a2 |
        }

    \addlyrics {
                Fi -- schia'il ven -- to, ur -- la la bu -- fe -- ra,
                sca -- rpe rot -- te ep -- pur bi -- so -- gna'andar,
        }
>>

refrain = \relative {

<<
    {
    \repeat volta 2 {
    
    %\mark "Refrain" 
    <<
    {
            \voiceOne
    e''4 <a f>8 <a f> <g e>4 <a f>8 <g e> | <f d> <f d> <e c> <d b> <e c>4 a, |
    <f' d>4. <d b>8 <e c>4. <c a>8 | <d b> <d b> <c a> <b gis> a2
    
    }
    \new Voice = "splitpart"
      {
        \voiceTwo
        c4 c8 c c4 c8 c | b b b b a4 a |
        b4. b8 a4. a8 | gis gis gis gis a2
    }>>
    \addlyrics {
                A con -- quis -- ta -- re la ro -- ssa pri -- ma -- ve -- ra
                Do -- ve sor -- ge'il sol de -- ll’av  -- ve -- nir.
        }
    }
    }  
>>
 
}

\score {
    \new ChoirStaff <<
        \new Voice = "sopranos" {
            \global 
            \intro 
            \break
            \refrain
            
        }
    >>
}

#(set-default-paper-size "a5landscape")
\paper {
left-margin = 0
right-margin = 30
indent=0
}
%#(set-global-staff-size 18)

keyfirst = {
  \key ees \major
  \time 4/4
}

leadMusic = \relative {
    \tempo 4 = 80
    
    % A
    \partial 2 r8 g' c ees 
    \mark \default \bar ".|:" g4 g8 g f g aes f ees c4 r8 r8 g c ees
    g8. g16 g8 g f g aes8. g16 f4. r8 r4 c8 ees
    f4 c8 ees f f g8. f16
    ees8 c r ees c ees c ees
    g8. g16 g8 aes g f d ees
    c4. r8 r2
    
    % B
    \mark \default g'4 ees c g8 g'
    (g) g2 r8 r4
    g4 f e c8 aes'
    (aes) (aes2) r8 r4
    f4 ees d c8 aes'8
    (aes) g r ees c ees c g'
    (g)   f r d b d b c
    (c) (c4) r8 r8 g c ees \bar ":|."
}

secondMusic = \relative {
    % A
    \partial 2 r8 g c d 
    \mark \default ees4 ees8 ees d ees f d c g4 r8 r8 g c d
    ees8. ees16 ees8 ees e e e8. e16 <f aes>4. r8 r4 f8 ees
    d4 f8 ees d d ees8. d16
    ees8 c r ees ees ees d c
    d8. b16 b8 c d d c b
    c4. r8 r2
    
    c4 c c c8 c
    (c8) c2 r8 r4
    c4 c e c8 f
    (f8) (f2) r8 r4
    d4 ees f d8 ees
    (ees) c r ees ees d c d
    (d) b r c d c b c
    (c) (c4) r8 r8 g c d 
}

leadWords = \lyricmode {
    % A
    Eu fui fa -- zer u -- ma ca -- sa de fa -- ri -- nha
    Bem ma -- nei -- ri -- nha que'o ven -- to pos -- sa le -- var
    Pas -- sa sol, pas -- sa chu -- va, pas -- sa ven -- to
    Nao pas -- sa'o mo -- vi -- men -- to do ci -- ran -- dei -- ro'a ro -- dar

    A -- chei bom, bo -- ni -- to
    Meu a -- mor brin -- car __
    Ci -- ran -- da ma -- ne -- ra
    Vem ca’ ci -- ran -- dei -- ra
    Vem ca’ ba -- lan -- çar __ 
    }
                

                
\score {
    \new ChoirStaff <<
        \new Staff = "lead" <<
            \new Voice = "sopranos" {\keyfirst << \leadMusic \secondMusic>>}
        >>
        \new Lyrics = "leadLyrics" 
        
        %\new Staff = "second" <<
        %    \new Voice = "altis" {\keyfirst \secondMusic}
        %>>
        
        \context Lyrics = "leadLyrics" \lyricsto "sopranos" \leadWords
    >>
}


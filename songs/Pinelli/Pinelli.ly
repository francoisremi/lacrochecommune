#(set-default-paper-size "a5landscape")
\paper {
left-margin = 0
right-margin = 30
indent=0
}
%#(set-global-staff-size 18)

keyfirst = {
  \key d \major
  \time 6/4
}

sopr = \relative {
    \tempo 2. = 35
    
    \partial 4 <b' fis>8 <d b> \bar ".|:" <fis d>4 <fis d>4. <fis d>8  <fis d>4 <e cis>4. <fis d>8 <g e>4 <g e> r
    r2 <g cis,>8 <a d,> <b e,>4 <b e,>4. <b e,>8 <b e,>4 <a fis>4. <g e>8 <g e>4 <fis d> r
    r2 <b d,>8 <b d,> <b d,>4 <fis d>4. <g d>8 <a d,>4 <g d>4. <fis d>8 <e d>4 cis r
    r2 <e cis>8 <e cis> <e cis>4 cis4. <d cis>8 <e cis>4 <d b>4. <cis a>8 <d b>4 r2
    r2 <b fis>8 <d b> \bar ":|."
}

%sopr2 = \relative {
%    \partial 4 b'8 d \bar ".|:" fis4 fis4. fis8  fis4 e4. fis8 g4 g r
%    r2 g8 a b4 b4. b8 b4 a4. g8 g4 fis r
%    r2 b8 b b4 fis4. g8 a4 g4. fis8 e4 cis r
%    r2 e8 e e4 cis4. d8 e4 d4. cis8 d4 r2
%    r2 b8 d \bar ":|."   
%}

%alt = \relative {
%    \partial 4 fis'8 b d4 d4. d8  d4 cis4. d8 e4 e r
%    r2 cis8 d e4 e4. e8 e4 fis4. e8 e4 d r
%    r2 d8 d d4 d4. d8 d4 d4. d8 d4 cis r
%    r2 cis8 cis cis4 cis4. cis8 cis4 b4. a8 b4 r2
%    r2 fis8 b
%}

alignermusic = \relative {
    \partial 4 b'8 d fis4 fis4. fis8  fis4 e4. fis8 g4 g r
    r2 g8 a b4 b4. b8 b4 a4. g8 g4 fis r
    r2 b8 b b4 fis4. g8 a8 a g4. fis8 e4 cis r
    r2 e8 e e4 cis4. d8 e4 d4. cis8 d4 r2
    r2 b8 d 
}

bass = \relative {
    \partial 4 fis,8 fis b4 b4. b8 b4 b4. b8 e4 e r
    r2 cis8 d e4 e4. e8 e4 d4. cis8 cis4 b r
    r2 fis8 fis b4 b4. b8 b4 b4. b8 fis4 fis r 
    r2 fis8 fis a4 a4. a8 fis4 fis4. fis8 b4 r2
    r2 fis8 fis
}

leadwords = \lyricmode {
    Quel -- la sera a Mi -- la -- no era cal -- do
    Ma che cal -- do, che cal -- do fa -- ce -- va,
    ``Bri -- ga -- diere, a -- pri un po’ la fi -- ne -- stra!'',
    U -- na spi -- nta Pi -- ne -- lli va giú.
    
    ``Sor que
}

\score {
    \new ChoirStaff <<
        \new Staff = "lead" <<
            \new Voice = "sopranos" {\keyfirst \sopr}
            %\new Voice = "sopranos" {\keyfirst <<\sopr2 \alt2>>}
            \new NullVoice = "aligner" \alignermusic
        >>
        \new Lyrics = "leadlyrics" 
           
        \new Staff <<
            \clef bass
            \new Voice = "basses" {\keyfirst  \bass}
        >>
        
        \context Lyrics = "leadlyrics" \lyricsto "aligner" \leadwords
    >>
}


#(set-default-paper-size "a5landscape")
\paper {
left-margin = 0
right-margin = 30
indent=0
}
%#(set-global-staff-size 18)

global = {
  \key bes \major
  \time 2/2
}

intro = <<
    \relative {
    % intro
        \mark "Intro"
        d''4 bes8 c d4 g f2 f |
        f,4 bes8 c d4 ees c2 r |
        g'4 ees8 f g4 a f4. d8 f2 |
        r8 ees c d ees4 f bes,2

        % anacrouse refrain
        
        r8 f'  <ees f> f 
        }

    \addlyrics {
                Se -- mo de Cin -- tu -- ri -- ni
                La -- scia -- te -- ce pa -- ssà
                Se -- mo be -- lle sim -- ba -- ti -- che
                Ce fa -- mo ris -- pe -- ttà
                Ma -- ti -- na
        }
>>

refrain = \relative {

<<
    {
    \repeat volta 2 {
    
    \mark "Refrain" <c'' f>4 <bes f'> r8 f' <ees f> f |
    <c f>2 r8 <f a> <f a> <f a> |
    <ees g>8. <d f>16 <c ees>8 <f a> <ees g>8. <d f>16 <c ees>8 <d f> |
    }
    \alternative{
        {<bes d>2 r8 f' <ees f> f}{}
    } \bar ":|."
    }
    \addlyrics {
    se -- ra, ti -- cche -- te -- ttà  
    In -- fi -- nu sa -- ba -- du ce to -- cca d’a -- bbo -- zzà
    Ma -- ti -- na
    }  
    
\new Staff \relative 
    { \key bes \major
    <<
        {
            \voiceOne
            r8 bes' d g f f r4 |
            r8 bes, d g f r8 r4 |
            f8. f16 f8 f f8. f16 f8 f |
            f2 r
        }
        %\addlyrics {
        %Ma -- ti -- na se -- ra,  ti -- cche -- te -- ttà
        %}  
        
        \new Voice = "splitpart"
        {
        \voiceTwo
        d,4 d8 d d4 f | 
        ees ees8 d ees4 r | 
        ees8. ees16 ees8 ees ees8. ees16 ees8 ees |
        d2 r
        }
        \addlyrics {
        Ma -- ti -- na se -- ra,  ti -- cche -- te -- ttà
        sa -- ba -- du ce to -- cca d’a -- bbo -- zzà
        }  
    >>
    }
    
>>
 
}

couplet = <<\relative {
        \break
         \set Score.repeatCommands = #'((volta "2")) 
         <bes' d>2 r4 \mark "Couplet" f8  bes 
         \set Score.repeatCommands = #'((volta #f))

 d2  (d8) c bes g |
f2 f4 d'8 ees |
f4 f8 f g4 a |
g8 ees4. r4 ees8 f |
g2 (g8) ees d ees |
c2 c4 f8 g |
f4 d c d |
bes2 r8 \mark "Refrain..." f' <ees f> f \bar "|."



}
\addlyrics{
zzà
    
Qua -- nno fi -- schia la si -- re -- na
Pri -- ma innan -- zi che fa -- ccia jur -- nu
Ce sen -- ti -- te atturnu a -- ttur -- nu
Den -- tre Ter -- ni da pa -- ssà   

Ma -- ti -- na
}
>>




\score {
    \new ChoirStaff <<
      %\new Staff = "lead" <<
        \new Voice = "sopranos" {
            \global 
            \intro 
            \break
            \refrain
            
            \couplet
        }
    >>
}

\paper {
  top-system-spacing.basic-distance = #10
  score-system-spacing.basic-distance = #20
  system-system-spacing.basic-distance = #15
  last-bottom-spacing.basic-distance = #10
}

\version "2.20.0"

\header {
  title = "Cinturini"
  composer="Anonyme"
}



global = {
  \key bes \major
  \time 2/2
}

sopMusic = \relative {
    
        % intro
        \mark "Intro"
        d'4 bes8 c d4 g f2 f |
        f,4 bes8 c d4 ees c2 c |
        g'4 ees8 f g4 a f4. d8 f2 |
        r8 ees c d ees4 f bes,2

        % refrain
        
r8 f'  <ees f> f |
\break


<<
    {
    \repeat volta 2 {
    
    \mark "Refrain" <c f>4 <bes f'> r8 f' <ees f> f |
    <c f>2 r8 <f a> <f a> <f a> |
    <ees g>8. <d f>16 <c ees>8 <f a> <ees g>8. <d f>16 <c ees>8 <d f> |
    }
    \alternative{
        {<bes d>2 r8 f' <ees f> f}
        {<bes, d>2 r4 f8  bes}
    }
    }
\new Staff \relative 
    { \key bes \major
    <<
        {
            \voiceOne
            r8 bes' d g f f r4 |
            r8 bes, d g f f r4 |
            f8. f16 f8 f f8. f16 f8 f |
            f2 r
        }
        \new Voice = "splitpart"
        {
        \voiceTwo
        d,4 d8 d d4 f | 
        ees ees8 d ees4 r | 
        ees8. ees16 ees8 ees ees8. ees16 ees8 ees |
        d2 r
        }
    >>
    }
>>


\break

\mark "Couplet" d2  (d8) c bes g |
f2 f4 d'8 ees |
f4 f8 f g4 a |
g8 ees4. r4 ees8 f |
g2 (g8) ees d ees |
c2 c4 f8 g |
f4 d c d |
bes2 r8 f' <ees f> f 

}

alignerMusic = \relative {
    a4. a'4 b8 c4. a4 f'8 e4. d8 e f e4( c8) a4.
    a4. b4 c8 d4. b4 f'8 e4. d8 e f e4( c8) a4. 
    a4. a8 b8 c8 c4. a8( b) c c4. a8 b c d4( c8) b4. r4.
    d4 f8 e4. d8( e) f e4 e8 d f e a,4. r4.
}

leadWordsOne = \lyricmode {
_ Su fra -- te -- lli pu -- gna -- mo _ da for -- ti
_ Con -- troi vi -- li ti -- ra -- nni _ bor -- ghe -- si
_ Ma co -- me fe -- ce Ca -- se -- rio e com -- pa -- gni
Che la mor -- te l’an -- die -- de a in -- con -- trà.
}

leadWordsTwo = \lyricmode {
Non _ vo -- glia -- mo più ser -- vi e pa -- dro -- ni
L’e _ -- gua -- glian -- za so -- cia -- le _ vo -- glia -- mo
_ Ma quel -- le ter -- re che noi la _  -- vo -- ria -- mo
A noi tu -- tti le spe -- se _ _ ci fa.
}

leadWordsThree = \lyricmode {
La _ mia tes -- ta schia -- ccia -- te _ la pu -- re
Di _ -- sse Caserio a -- gli -- in -- qui -- si -- si suo __ i
Ma _ l’a -- nar -- chia è più for -- te _ de tuo -- i
Pres -- to pre -- sto shia -- cci -- ar -- vi _ do -- vrà.
}

bassMusic = \relative {
  a,4.~ a4 a8 g4. g4 g8 gis4. gis8 gis gis a4. a4.
  a4.~ a4 a8 g4. g4 g8 gis4. gis4 gis8 a4. a4. 
  <<
{
      \voiceOne
      r4.
  }
  \new Voice = "splitpart"{
      \voiceTwo
      a4. 
}

>>
\oneVoice
  a8 a a g4. g4 g8 fis4. fis4 fis8 e4. e4. r4.
  
  e4 e8 a4. a4 a8 e4. e4 e8 a4. r4.
}


\score {
    \new ChoirStaff <<
      %\new Staff = "lead" <<
        \new Voice = "sopranos" {\global \sopMusic}
    >>
}

%\markup{Couplet 1 : unisson voix lead, \dynamic p}

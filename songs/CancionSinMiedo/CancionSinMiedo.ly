#(set-default-paper-size "a5landscape")
\paper {
left-margin = 0
right-margin = 30
indent=0
}
%#(set-global-staff-size 18)

keyfirst = {
  \key d \major
  \time 12/8
}

%\set Score.rehearsalMarkFormatter = #format-mark-box-alphabet

leadMusic = \relative {
    \tempo 4. = 70
    
    % A
     \partial 2. r4 fis'8 d' cis d \mark \default cis b fis d' cis d cis b 
     b b cis d fis fis fis fis e16 d fis8 fis r
     fis fis g fis e e e e d16 cis e8 e r
     e e8 fis e d4. cis r4
     cis8 fis cis d cis b4 (b4.)
     
     % B
     r4 fis8 d' cis d \mark \default cis b fis d' cis d cis b 
     b b cis d fis fis fis fis e16 d fis8 fis r
     fis fis g fis e e e e d16 cis e8 e r
     e e fis e d4 d8 d4 d8 cis r fis, fis d' cis cis b4 (b4.)
     
     % C
     r4 fis8 d' cis d \mark \default cis \mark \markup { \musicglyph "scripts.segno" }  b fis d' cis d cis b
     b b cis d fis fis fis fis e16 d fis8 fis r
     fis fis g fis e e e e d cis e16 e r8
     e e fis e d d d d fis d cis cis 
     
     % D
     fis, d' cis d \mark \default cis b fis d' cis d cis b r16 
     b b8 cis d fis fis fis fis e16 d fis8 fis r8
     fis fis g fis e e e e16 d cis e e8 r4
     e8 e fis e d4. cis4. r4
     cis8 fis cis d \mark \markup { \musicglyph "scripts.coda" } cis b4 (b4.)
     
     % E
     r4 fis8 d' cis d \mark \default cis b fis d' cis d cis b 
     b b cis d fis fis fis fis e16 d fis8 fis r
     fis fis g fis e e e e d16 cis e8 e r
     e e fis e d4. (d4 d8) cis4
     cis16 cis fis4 d8 fis fis4 (fis4.) \glissando (b4.) r4
     
     % F
     fis8 \mark \default fis4 d8 fis4 d8 fis4 d8 fis4 d8 fis4 e8 (e2.) r4
     fis8 g4 e8 g4 e8 g4 e8 g4 e8 b'4 fis8 (fis4. cis) r8
     fis fis fis4 d8 fis4 d8 fis4 d8 fis4 d8 e4 e8 (a2.) r8
     g8 fis g4 g8 g4 g8 fis4 cis8 e4 d8 cis4 b2. r4
     b8 cis d4 d8 d4 d8 fis4 fis,8 d'4 cis8 cis4 b8 (b4.) r8 
     
     % Justicia !
     \override NoteHead.style = #'cross
     fis8 fis \autoBeamOff fis \autoBeamOn b b
     \autoBeamOff b \autoBeamOn d d d r4 r4
     \revert NoteHead.style
     
     fis,8 d' cis \mark \markup { \musicglyph "scripts.segno" } d \bar "||"
     
     \mark \markup { \musicglyph "scripts.coda"} cis b4 (b4.) r4
     e8 e fis e d4. cis4. r4
     cis8 fis cis d cis b4 (b4.) r2
     
     % G
     fis8 fis \mark \default b b \mark "lent et libre" b cis b cis d b4 r8
     cis8. d16 fis8 fis fis e d cis b4. r8
     fis fis b b b cis b cis d b4 r8 
     cis8. d16 fis8 fis fis e d fis b,2. \bar "|."
}

accMusic = \relative {
% A
\partial 2. r2. fis'4 r8 fis4 r8 fis4 r8 r4.
fis4 r8 a4 r8 fis4 r8 r4.
g4 r8 g4 r8 g4 r8 r4.
<<
    {
        \voiceOne 
        g2. (fis4.)
    }
    {
        \voiceTwo 
        b4. d cis
    }
>> \oneVoice r4.
r1.

% B
fis,4 r8 fis4 r8 fis4 r8 r4.
fis4 r8 a4 r8 fis4 r8 r4.
g4 r8 g4 r8 g4 r8 g4 r8
r4. b4 b8 ais8 r ais ais ais ais <b fis> <b fis>4 (<b fis>4.) r2.

%C
fis2. (<b fis> <a fis> fis)
<e g>4. (<fis a> <g b> <a cis> <b d>2. <ais cis>4)

% D
<ais cis>8 <ais cis> <ais cis> <ais cis> <b d> <b d> <b d> <b d> <b d> <b d> <b d> <b d> r16 
<a cis> <a cis>8 <a cis> <a cis> <d fis> <d fis> <d fis>  <d fis> <cis e>16 <b d> <d fis>8 <d fis> r8
fis fis g fis e e e e16 d cis e e8 r4
e8 e fis e g4. (b ais4)
ais8 ais ais ais b b4 (b4.)

% E
r4 fis,8 d' cis d cis b fis d' cis d cis b 
b b cis d fis fis fis fis e16 d fis8 fis r
fis fis g fis e e e e d16 cis e8 e r
e e fis e d4. (b4 b8) cis4
cis16 cis fis4 cis8 d b4 (b4.) \glissando (b'4.) r4.

% F
a2. (a4. b cis2. cis4.) r4.
b (cis d cis b2. a4.)
r8 a a a4 fis8 a4 fis8 a4 fis8 a4 fis8 g4 g8 (a2.) r8
g8 fis g4 g8 g4 g8 fis4 cis8 e4 d8 cis4 b2. r4
b8 cis d4 d8 d4 d8 fis4 fis,8 d'4 cis8 cis4 b8 (b4.) r8

% Justicia !
\override NoteHead.style = #'cross
fis8 fis \autoBeamOff fis \autoBeamOn b b
\autoBeamOff b \autoBeamOn d d d r4
\revert NoteHead.style

r2.
b8 b4 (b4.) r4
e8 e fis e g4. (b ais4)
ais8 ais ais ais b b4 (b4.) r2.
}

leadWords = \lyricmode {
                % A
                Que tiem -- ble'el Es -- ta -- do los cie -- los, las cal -- les
                Que tiem -- blen  los jue -- ces y los ju -- di -- cia -- les,
                Hoy a las mu -- je -- res nos qui -- tan la cal -- ma
                Nos sem -- bra -- ron mie -- do, nos cre -- cie -- ron a -- las __
                
                % B
                A ca -- da mi -- nu -- to, de ca -- da se -- ma -- na,
                nos ro -- ban a -- mi -- gas, nos ma -- tan her -- ma -- nas,
                des -- tro -- zan sus cuer -- pos, los des -- a -- pa -- re -- cen
                No'ol -- vi -- de sus num -- bres, por fa -- vor, se -- ñor pres -- si -- den -- te __
                
                % C
                Por to -- das las com -- pas mar -- chan -- do'en Re -- for -- ma
                Por to -- das las mor -- ras pe -- lean -- do'en So -- no -- ra
                Por las com -- an -- dan -- tas lu -- chan -- do por Chia -- pas
                Por to -- das las ma -- dres bus -- can -- do'en Ti -- juia -- na
               
                % D
                Can -- ta -- mos sin mie -- do, pe -- di -- mos jus -- ti -- cia,
                Gri -- ta -- mos por ca -- da des -- a -- pa -- re -- ci -- da
                Que re -- sue -- ne fuer -- te: ``NOS QUE -- RE -- MOS VI -- VAS''!
                Que cai -- ga con fue -- rza, el fe -- mi -- ni -- ci -- da __
                
                % E
                Yo to -- do lo'in -- cen -- dio, yo to -- do lo rom -- po
                Si'un día'al -- gún ful -- a -- no te'a -- pa -- ga los o -- jos
                Ya na -- da me cal -- la, ya to -- do me so -- bra
                Si to -- can a u __ na, res -- pon -- de -- mos to -- das __
                
                % F
                Soy Clau -- dia, soy Es -- ther y soy Te -- re -- sa
                Soy In -- grid, soy Fa -- bio -- la'y soy Va -- le -- ria __
                Soy la ni -- ña que sub -- si -- ste por la fuer -- za __
                Soy la ma -- dre que'aho -- ra''ll -- o -- ra por sus muer -- tas
                Y soy e -- sta que te'ha -- rá pa -- gar las cuen -- tas __
                Jus -- ti -- cia! Jus -- ti -- cia! Jus -- ti -- cia!
                
                % reprise
                Por to -- das las 
                ci -- da __
                Que cai -- ga con fue -- rza, el fe -- mi -- ni -- ci -- da __
                
                % G
                Y re -- tiem -- blen sus cen -- tros la tier -- ra
                Al so -- ro -- ro ru -- gir del a -- mor
                Y re -- tiem -- blen sus cen -- tros la tier -- ra
                Al so -- ro -- ro ru -- gir del a -- mor
                }

accWords = \lyricmode{
        % A
        ou ou ou
        ou ou ou
        ou ou ou
        ah __
        
        % B
        ou ou ou
        ou ou ou
        ou ou ou ou
        por fa -- vor, se -- ñor pres -- si -- den -- te __
        
        % C
        ah __ __ __ 
        ah __ __ __ __ __
        
        % D
        Can -- ta -- mos sin mie -- do, pe -- di -- mos jus -- ti -- cia,
        Gri -- ta -- mos por ca -- da des -- a -- pa -- re -- ci -- da
        Que re -- sue -- ne fuer -- te: ``NOS QUE -- RE -- MOS VI -- VAS''!
        Que cai -- ga con ah __ __ el fe -- mi -- ni -- ci -- da __
        
        % E
        Yo to -- do lo'in -- cen -- dio, yo to -- do lo rom -- po
        Si'un día'al -- gún ful -- a -- no te'a -- pa -- ga los o -- jos
        Ya na -- da me cal -- la, ya to -- do me so -- bra
        Si to -- can a u __ na, res -- pon -- de -- mos to -- das __
        
        % F
        ah __ ah __
        %Soy la ni -- ña que sub -- si -- ste por la fuer -- za __
        %Soy la ma -- dre que'aho -- ra''ll -- o -- ra por sus muer -- tas
        %Y soy e -- sta que te'ha -- rá pa -- gar las cuen -- tas __
        %Jus -- ti -- cia! Jus -- ti -- cia! Jus -- ti -- cia!
}
                
\score {
    \new ChoirStaff <<
        \new Staff = "lead" <<
            \new Voice = "sopranos" {\keyfirst \leadMusic}
        >>
        \new Lyrics = "leadLyrics" 
           
        \new Staff <<
            \new Voice = "accompagnements" {\keyfirst  \accMusic}
        >>
        \new Lyrics = "accLyrics" 
        
        \context Lyrics = "leadLyrics" \lyricsto "sopranos" \leadWords
        \context Lyrics = "accLyrics" \lyricsto "accompagnements" \accWords
    >>
}


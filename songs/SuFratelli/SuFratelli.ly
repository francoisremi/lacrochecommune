\paper {
  top-system-spacing.basic-distance = #10
  score-system-spacing.basic-distance = #20
  system-system-spacing.basic-distance = #13
  last-bottom-spacing.basic-distance = #10
}

\version "2.20.0"

\header {
  title = "Su Fratelli"
  composer="Anonyme"
}

global = {
  \key c \major
  \time 6/8
}

sopMusic = \relative {
    r4.^\markup{\huge \box A} a'4_\markup {\dynamic p} b8 c4. a4 f'8 e4. d8 e f e4( c8) a4.
    r4. b4 c8 d4. b4 f'8 e4. d8( e) f e4( c8) a4. r4 a8
    b4 c8 c4. a8( b) c c4. a8 b c d4( c8) b4. r4.
    d4 f8 e4. d8( e) f e4 e8 d f e a,4. r4.
    
    r4.^\markup{\huge \box B} a4 b8 c4. a4 f'8 e4. d8 e f e4( c8) a4.
    r4. b4 c8 d4. b4 f'8 e4. d8( e) f e4( c8) a4. r4 a8
    b4 c8 c4. a8( b) c c4. a8 b c d4( c8) b4. r4.
    d4 f8 e4. d8( e) f e4 e8( d f) e a,4. r4.
    
    \repeat volta 2 { r4._\markup {
  \italic \small { 2nd } \hspace #0.1 \dynamic f
}^\markup{\huge \box C} a4 b8 c4. a4 f'8 e4. d8 e f e4( c8) a4.
    r4. b4 c8 d8 d d b4 f'8 e4 e8 d8( e) f e4( c8) a4. r4 a8
    b4 c8 c4. a8( b) c c4. a8 b c d4( c8) b4.^\markup{\italic \small{2nd}} r4.\fermata_\markup {
  \italic \small { 2nd } \hspace #0.1 \dynamic fff
}
    d4 f8 e4 e8 d8 e f e4. d8 (f e) a,4. r4.}
}
leadWordsOne = \lyricmode {
  Su fra -- te -- lli pu -- gna -- mo _ da for -- ti
  Con -- troi vi -- li ti -- ra -- nni bor -- ghe -- si
  Ma co -- me fe -- ce Ca -- se -- rio e com -- pa -- gni
  Che la mor -- te l’an -- die -- de a in -- con -- trà.
  
    Non vo -- glia -- mo più ser -- vi e pa -- dro -- ni
L’e -- gua -- glian -- za so -- cia -- le vo -- glia -- mo
Ma quel -- le ter -- re che noi la _  -- vo -- ria -- mo
A noi tu -- tti le spe -- se __ ci fa.

La mia tes -- ta schia -- ccia -- te _ la pu __ re
Di -- sse Ca -- se -- rio a -- gli -- in -- qui -- si -- si suo __ i
Ma l’a -- nar -- chia è più for -- te _ de tuo -- i
Pres -- to pres -- to shia -- cci -- ar -- vi do __ vrà.
}

leadWordsTwo = \lyricmode {
  Non vo -- glia -- mo più ser -- vi e pa -- dro -- ni
L’e -- gua -- glian -- za so -- cia -- le vo -- glia -- mo
Ma quel -- le ter -- re che noi la _  -- vo -- ria -- mo
A noi tu -- tti le spe -- se _ _ ci fa.
}

leadWordsThree = \lyricmode {
  Non vo -- glia -- mo più ser -- vi e pa -- dro -- ni
L’e -- gua -- glian -- za so -- cia -- le vo -- glia -- mo
_ Ma l’a -- nar -- chi -- a è più for -- te _  de tuo _ -- i
A noi tu -- tti le spe -- se _ _ ci fa.
}

bassMusic = \relative {
r4. a,4_\markup {\dynamic p} b8 c4. a4 f'8 e4. d8 e f e4( c8) a4.
r4. b4 c8 d4. b4 f'8 e4. d8( e) f e4( c8) a4. r4 a8
b4 c8 c4. a8( b) c c4. a8 b c d4( c8) b4. r4.
d4 f8 e4. d8( e) f e4 e8 d f e a,4. r4.
  
  a4.~ a4 a8 g4. g4 g8 gis4. gis8 gis gis a4. a4.
  a4.~ a4 a8 g4. g4 g8 gis4. gis4 gis8 a4. a4. 
  r4 a8 a4 a8 g4. g4 g8 f4. f4 f8 e4. e4. r4.
  e4 e8 a4. a4 a8 e4. e4 e8 a4. r4.
  
  \repeat volta 2 {a4.~_\markup {
  \italic \small { 2nd } \hspace #0.1 \dynamic f
} a4 a8 g4. g4 g8 gis4. gis4 gis8 a4. a4.
  a4.~ a4 a8 g8 g8 g8 g4 g8 gis4. gis8 gis8 gis8 a4. a4. 
  a4. a4 a8 g4. g4 g8 f4. f4 f8 e4. e4.^\markup{\italic \small{2nd}} r4.\fermata_\markup {
  \italic \small { 2nd } \hspace #0.1 \dynamic fff
}
  e4 e8 a4. a4 a8 e4. e4 e8 a4. r4. } 
  
}
bassWords = \lyricmode {
Su fra -- te -- lli pu -- gna -- mo _ da for -- ti
Con -- troi vi -- li ti -- ra -- nni bor -- ghe -- si
Ma co -- me fe -- ce Ca -- se -- rio e com -- pa -- gni
Che la mor -- te l’an -- die -- de a in -- con -- trà.    

Non vo -- glia -- mo più ser -- vi e pa -- dro -- ni
L’e -- gua -- glian -- za so -- cia -- le vo -- glia -- mo
Ma quel -- le ter -- re che noi la  -- vo -- ria -- mo
A noi tu -- tti le spe -- se ci fa.

La mia tes -- ta schia -- ccia -- te la pu -- re
Di -- sse Ca -- se -- rio a -- gli -- in -- qui -- si -- si suo __ i
Ma l’a -- nar -- chia è più for -- te de tuo -- i
Pres -- to pres -- to shia -- cciar -- vi do vrà.
}



\score {
  \new ChoirStaff <<
    
    %\with {
      % this is needed for lyrics above a staff
      %  \override VerticalAxisGroup.staff-affinity = #DOWN
  %}
    \new Staff = "women" <<
      \new Voice = "sopranos" {
        << \global \sopMusic >>
      }
    >>
    \new Lyrics = "leadOne" 
    %\new Lyrics = "leadTwo" 
    %\new Lyrics = "leadThree" 
    
    
    \new Staff = "men" <<
      \clef bass
      \new Voice = "basses" {
        << \global \bassMusic >>
      }
    >>
    \new Lyrics = "basses"
    \context Lyrics = "leadOne" \lyricsto "sopranos" \leadWordsOne
    %\context Lyrics = "leadTwo" \lyricsto "sopranos" \leadWordsTwo
    %\context Lyrics = "leadThree" \lyricsto "sopranos" \leadWordsThree
    
    \context Lyrics = "basses" \lyricsto "basses" \bassWords
  >>
}

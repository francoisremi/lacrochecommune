\paper {
  top-system-spacing.basic-distance = #10
  score-system-spacing.basic-distance = #20
  system-system-spacing.basic-distance = #15
  last-bottom-spacing.basic-distance = #10
}

\version "2.20.0"

\header {
  title = "Su Fratelli"
  composer="Anonyme"
}



global = {
  \key c \major
  \time 6/8
}

sopMusic = \relative {
    \repeat volta 2 {
        
        r4. a'4 b8 c4. a4 f'8 e4. d8 e f e4( c8) a4.
    r4. b4 c8 d4. b4 f'8 e4. 
    \set melismaBusyProperties = #'()
    d8( e f)
    \unset melismaBusyProperties
    e4( c8) a4. 
    r4. a8 b8 c8 c4. a8( b) c c4. a8 b c d4( c8) b4. r4.
    d4 f8 e4. d8( e) f e4 e8 d f e a,4. r4. 
    }
}

alignerMusic = \relative {
    a4. a'4 b8 c4. a4 f'8 e4. d8 e f e4( c8) a4.
    a4. b4 c8 d4. b4 f'8 e4. d8 e f e4( c8) a4. 
    a4. a8 b8 c8 c4. a8( b) c c4. a8 b c d4( c8) b4. r4.
    d4 f8 e4. d8( e) f e4 e8 d f e a,4. r4.
}

leadWordsOne = \lyricmode {
_ Su fra -- te -- lli pu -- gna -- mo _ da for -- ti
_ Con -- troi vi -- li ti -- ra -- nni _ bor -- ghe -- si
_ Ma co -- me fe -- ce Ca -- se -- rio e com -- pa -- gni
Che la mor -- te l’an -- die -- de a in -- con -- trà.
}

leadWordsTwo = \lyricmode {
Non _ vo -- glia -- mo più ser -- vi e pa -- dro -- ni
L’e _ -- gua -- glian -- za so -- cia -- le _ vo -- glia -- mo
_ Ma quel -- le ter -- re che noi la _  -- vo -- ria -- mo
A noi tu -- tti le spe -- se _ _ ci fa.
}

leadWordsThree = \lyricmode {
La _ mia tes -- ta schia -- ccia -- te _ la pu -- re
Di _ -- sse Caserio a -- gli -- in -- qui -- si -- si suo __ i
Ma _ l’a -- nar -- chia è più for -- te _ de tuo -- i
Pres -- to pre -- sto shia -- cci -- ar -- vi _ do -- vrà.
}

bassMusic = \relative {
  a,4.~ a4 a8 g4. g4 g8 gis4. gis8 gis gis a4. a4.
  a4.~ a4 a8 g4. g4 g8 gis4. gis4 gis8 a4. a4. 
  <<
  { 
      \voiceOne
      r4.
  }
  \new Voice = "splitpart"{
      \voiceTwo
      a4. 
  }
>>
\oneVoice
  a8 a a g4. g4 g8 fis4. fis4 fis8 e4. e4. r4.
  
  e4 e8 a4. a4 a8 e4. e4 e8 a4. r4.
}


\score {
  \new ChoirStaff <<
    \new Staff = "lead" \transpose a d <<
        \new Voice = "sopranos" {\global \sopMusic}
      \new NullVoice = "aligner" \alignerMusic
    >>
    \new Lyrics = "leadOne" 
    \new Lyrics = "leadTwo" 
    \new Lyrics = "leadThree" 
    
    
    \new Staff = "bass" \transpose a d' <<
      \clef bass
      \new Voice = "basses" {\global \bassMusic}
    >>
    \new Lyrics = "bassesOne"
    \new Lyrics = "bassesTwo"
    \new Lyrics = "bassesThree"
    
    \context Lyrics = "leadOne" \lyricsto "aligner" \leadWordsOne
    \context Lyrics = "leadTwo" \lyricsto "aligner" \leadWordsTwo
    \context Lyrics = "leadThree" \lyricsto "aligner" \leadWordsThree

  >>
}

\markup{Couplet 1 : unisson voix lead, \dynamic p}
\markup{Couplet 2 : voix lead + voix basse, \dynamic p}
\markup{Couplet 3 : voix lead + voix basse, \dynamic f}
\markup{Couplet 3bis : voix lead + voix basse à l'octave sup, \dynamic fff}
\markup{"                        faire une pause puis répéter la dernière phrase."}

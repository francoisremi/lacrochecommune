#(set-default-paper-size "a5landscape")
\paper {
left-margin = 0
right-margin = 30
indent=0
}
%#(set-global-staff-size 18)

keyfirst = {
  \key g \major
  \time 6/8 
}

intro = <<\relative {
e'4.~ 8 b'8 a 
  b4.~ 8 a g
  a4.~ 8 b c
  b4. r4.
  
  e,4.~ 8 <b' g>8 <a fis> 
  <b g>4.~ 8 <a fis> <g e>
  <a fis>4.~ 8 <b e,> <c d,>
  <b e,>4.\fermata r4
          }
          >>

sopr = {
   \relative {
  % intro
    \partial 8 
    b8  
  \repeat volta 2{
  e e e fis e fis
  \time 9/8
   g4 <b g>8 <d g,>4.~ 4 <b g>8
   \time 6/8
   <a fis> <a fis> <a fis>~ 8 <g e> <fis d>
   <g e>4.~ 4 
   <<
     {
       \voiceOne
   <b g>8
   <a fis >8 <a fis > <a fis >4 <b g>8 < c fis,>
     }
     \new Voice{
       \voiceTwo
       e,8 d8 d d4 <e b>8 <a, fis'>
     }
   >>
   \oneVoice
  }
  
  \alternative{
   {
      <<
     {
       \voiceOne
   <b' g>4.~ 4
     }
     \new Voice{
       \voiceTwo
       <e, b>4.~ 4
     }
   >>
   \oneVoice
   
    b8
   } 
   {
   <<
     {
       \voiceOne
   <b' g>2.
     }
     \new Voice{
       \voiceTwo
       <e, b>2.
     }
   >>
   \oneVoice
   }
   
  }
  
  
  \repeat volta 2 {
   <b' g>4 <b g>8
   <b g>4 <d d,>8 <b g>4.~
  <b g>4 <b g>8 <b g>4 <b g>8
  <b g> g <b g> 
  %<<
  % {
  %   \voiceOne
  %<b g> g <b g> 
  % }
  % \new Voice{
  %  \voiceTwo
  %  d,8 d d
  % }
  %>>
  %\oneVoice
  <d g,> <b g> g
  
  <<
  {\voiceOne
  <a fis>8 <a fis>4~
  }
  \new Voice{
    \voiceTwo
    <a fis>8 fis e
  }
  >>
  \oneVoice
  }
  \alternative{
   {
     <<
       {
         \voiceOne
     <a fis>4.  
       }
       \new Voice{
         \voiceTwo
         d,4.
       }
     >>
     \oneVoice
   r4.
   }
   {
     %<a fis>4.\repeatTie r8 b8 b 
   <<
       {
         \voiceOne
     <a' fis>4.\repeatTie r8 b8 b
       }
       \new Voice{
         \voiceTwo
         d,8 g fis e4.~
       }
     >>
     \oneVoice
   }
  }
  
  \repeat volta 2 {
  \time 9/8
  <<{
    \voiceOne
  b'4. 
    }
    \new Voice{
      \voiceTwo
      e,4.\repeatTie
    }
  >>
  \oneVoice
  
  <a fis>4.~ <a fis>4 <a fis>8 
  \time 6/8
  <a fis>8 <a fis> <a fis>
  <b g>4 
  <<
    {
      \voiceOne
  <c a>8 
    }
    \new Voice{
      \voiceTwo
     fis,8 
    }
  >>
  \oneVoice
  }
  \alternative{
   {
     <<
       {
         \voiceOne
     <b g>4. r8 b8 b 
       }
       \new Voice{
        \voiceTwo 
        e,8 g fis e4.
       }
     >>
     \oneVoice
   }
   {
     <<
       {\voiceOne
     <b' g>2.
       }
       \new Voice{
        \voiceTwo 
        e,2.
       }
     >>
     \oneVoice
       }
  }
  
  %
}
\bar "|."
}

alignermusicOne = \relative{
 a2.~ 2.~ 2.~ 4. r4. 
 a2.~ 2.~ 2.~ 4. r4  
 
 %Hegoak ebaki banizkio
 a8 a a a a a a
 a4 a8 d4. ~ 4
 
 %Neuria izango zen
 a8 a a a~ 8 a a a4.~ 4
 
 %Ez zuen alde egingo
 a8 a a a4 a8 a a4.~ 4 a8
 
 a2.
 
 %Bainan horrela
 a4 a8 a4 a8
 a4.~ 4 
 
 % Ez zuen gehiago xoria izango
 a8 a4 a8 a a a 
 a a a a8~ 8~ 8~ 4. r4.
 
 %Eta nik,
 r8 a a a4.~ a4.~ 4.~ 4
 
 %xoria nuen maite
 a8 
 a a a a4 a8
 a8
 
 %Eta nik,
 a a a8~ 8~ 8
 
 % te
 a2.
}

alignermusicTwo = \relative{
 r2. r r r 
 r2. r r r4. r4
 
 %Si je lui avais coupé les ailes
 a8 a a a a a a
 a4 a8 d4. ~ 4
 
 %il aurait été à moi
 a8 a4 a4 a8 a a4.~ 4
 
 % Il ne serait pas parti
 a8 a4 a4 a8 a a4.~ 4 a8
 
 a2.
 
 % Oui mais voilà
 a4. a4 a8
 a4.~ 4 
 
 % Il n'aurait plus été un oiseau
 a8 a4 a8 a8~ 8 a 
 a a a a8~ 8~ 8~ 4. r4.
 
 %Oui mais moi
 r8 a a a4.~ a4.~ 4.~ 4
 
 %c'est l'oiseau que j'aimais
 a8 
 a4 a8 a4 a8
 a8
 
 %Eta nik,
 a a a8~ 8~ 8
 
 % te
 a2.
}

wordsOne = \lyricmode {
    oh __ oh __
    He -- go -- ak e -- ba -- ki ba -- niz -- ki -- o __ 
    Neu -- ri -- a i -- zan -- go zen
    Ez zu -- en al -- de'e -- gin -- go __ 
    
    He-
    
    -go
    
    Bai -- nan hor -- re -- la __
    Ez zuen ge -- hia -- go xo -- ri -- a'i -- zan -- go __ 
    
    E -- ta nik __ 
    xo -- ri -- a nu -- en mai -- te
    E -- ta nik __
    -te
}

wordsTwo = \lyricmode{
    Si je lu -- i a -- vais cou -- pé les ailes
    Il au -- rait été à moi
    Il ne serait pas par -- ti
    
    Si
    
    -ti
    
    Oui mais voi -- là, 
    Il n’au -- rait plus __ é -- té un oi -- seau __
    
    Oui mais moi __
    
    c’est l’oi -- seau que j’ai -- mais
    
    Oui mais moi __
    -mais
}

\score {
    \new ChoirStaff <<
        \new Staff = "lead" <<
            \new Voice = "sopranos" {\keyfirst \intro \sopr}
            \new NullVoice = "alignerOne" \alignermusicOne
            \new NullVoice = "alignerTwo" \alignermusicTwo
            
        >>
        \new Lyrics \lyricsto "alignerOne" \wordsOne
        \new Lyrics \lyricsto "alignerTwo" \wordsTwo
        %\new Lyrics = "leadlyrics" 
           
        %\new Staff <<
        %    \clef bass
        %    \new Voice = "basses" {\keyfirst  \bass}
        %>>
        
        %\context Lyrics = "leadlyrics" \lyricsto "sopranos" \leadwords
    >>
}

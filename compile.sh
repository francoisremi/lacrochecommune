lilypond-book --pdf --format=latex --output=.compil_temp lacrochecommune.lytex --include=part
cp *.ttf .compil_temp/.
cd .compil_temp
xelatex lacrochecommune.tex
mv lacrochecommune.pdf ../lacrochecommune.pdf
cd ../
